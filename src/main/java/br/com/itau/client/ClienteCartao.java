package br.com.itau.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClienteCartaoConfiguracao.class)
public interface ClienteCartao {

    @GetMapping("/{id}")
    Cliente buscarClientePeloId(@PathVariable int id);
}
