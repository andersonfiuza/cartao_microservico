package br.com.itau.Model;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Cartao {
//
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @NotBlank(message = "Favor Informar o Numero do Cartão")
    private String numero;


    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }


//    private int idcliente;
//
//    public int getIdcliente() {
//        return idcliente;
//    }
//
//    public void setIdcliente(int idcliente) {
//        this.idcliente = idcliente;
//    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setClienteId(int id) {
    }


}
