package br.com.itau;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FOUND, reason = "Cartão já cadastrado!")
public class CartaoJaExiste extends  RuntimeException {
}
