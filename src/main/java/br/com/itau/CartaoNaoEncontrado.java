package br.com.itau;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Cartao não encontrado")
public class CartaoNaoEncontrado extends  RuntimeException{



}
