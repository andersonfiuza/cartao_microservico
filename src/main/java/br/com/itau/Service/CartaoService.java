package br.com.itau.Service;


import br.com.itau.CartaoJaExiste;
import br.com.itau.CartaoNaoEncontrado;
import br.com.itau.DTO.CartaoDTO;
import br.com.itau.DTO.CartaoSaidaDTO;
import br.com.itau.ErroAoAtivar;
import br.com.itau.Model.Cartao;
import br.com.itau.client.Cliente;
import br.com.itau.client.ClienteCartao;
import br.com.itau.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {


    @Autowired
    CartaoRepository cartaoRepository;


    @Autowired
    ClienteCartao clienteCartao;


    public CartaoSaidaDTO solicitaNovoCartao(CartaoDTO cartaoDTO) {

        Optional<Cartao> cartaoOptional = cartaoRepository.findFirstByNumero(cartaoDTO.getNumero());

        if (!cartaoOptional.isPresent()) {

            Cartao cartao = new Cartao();
            Cliente cliente = clienteCartao.buscarClientePeloId(cartaoDTO.getClienteId());



            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setClienteId(cartaoDTO.getClienteId());
            cartao.setAtivo(false);


            cartao = cartaoRepository.save(cartao);

            return new CartaoSaidaDTO(cartao, cliente);
        } else {
            throw new CartaoJaExiste();
        }

    }


    //Ativa /Desativa o cartão.
    public Cartao ativaCartaoCliente(String numero, Boolean status) {


        Cartao cartao = buscarCartaoPeloNumero(numero);

        try {
            cartao.setAtivo(status);

            return cartaoRepository.save(cartao);
        } catch (RuntimeException e) {

            throw new ErroAoAtivar();
        }


    }


    //Busca Cartao do cliente pelo numero.
    public Cartao buscarCartaoPeloNumero(String numero) {


        Optional<Cartao> cartaoOptional = cartaoRepository.findFirstByNumero(numero);

        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new CartaoNaoEncontrado();
        }

    }

    public Cartao buscarCartaoPeloId(int id) {
        Optional<Cartao> clienteOptional = cartaoRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cartao cartao = clienteOptional.get();
            return cartao;
        } else {
            throw new CartaoNaoEncontrado();
        }


    }
}
