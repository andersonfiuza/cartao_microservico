package br.com.itau.repositories;


import br.com.itau.Model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartaoRepository extends JpaRepository<Cartao, Integer> {

        Optional<Cartao> findFirstByNumero(String numero);
}
