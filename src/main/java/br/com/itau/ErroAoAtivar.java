package br.com.itau;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason = "Erro ao Ativar o Cartão")
public class ErroAoAtivar extends  RuntimeException{
}
