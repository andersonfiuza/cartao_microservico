package br.com.itau.Controller;


import br.com.itau.DTO.CartaoBloqueadoDTO;
import br.com.itau.DTO.CartaoDTO;
import br.com.itau.DTO.CartaoSaidaDTO;
import br.com.itau.ErroAoAtivar;
import br.com.itau.Model.Cartao;
import br.com.itau.Service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
//@RequestMapping("/cartoes")
public class CartaoController {


    @Autowired
    private CartaoService cartaoServer;


    @PostMapping("/solicitarnovocartao")
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO solicitaNovoCartao(@RequestBody CartaoDTO cartaoDTO) {

        return cartaoServer.solicitaNovoCartao(cartaoDTO);


    }


    //Buscar Cartão.
    @GetMapping(value = "/buscarpornumero/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPeloNumero(@RequestBody @PathVariable(name = "numero") String numero) {
        return cartaoServer.buscarCartaoPeloNumero(numero);


    }

    //Ativa/Desativa
    @PutMapping(value = "/ativacartao/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao ativaDesativaCartao(@RequestBody CartaoBloqueadoDTO status, @PathVariable(name = "numero") String numero) {

        return cartaoServer.ativaCartaoCliente(numero, status.getStatuscartao());

    }

    //Buscar cartalo pelo ID.
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPeloId(@PathVariable(name = "id") int id) {

        return cartaoServer.buscarCartaoPeloId(id);
    }


}
