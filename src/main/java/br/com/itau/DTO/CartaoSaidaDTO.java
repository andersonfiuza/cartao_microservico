package br.com.itau.DTO;

import br.com.itau.Model.Cartao;
import br.com.itau.client.Cliente;

public class CartaoSaidaDTO {


    private int id;

    private int clienteId;

    private String numero;

    private Boolean ativo;

    private Cliente cliente;

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoSaidaDTO(Cartao cartao, Cliente cliente) {

        this.setAtivo(cartao.getAtivo());
        this.setClienteId(cliente.getId());
        this.setNumero(cartao.getNumero());
        this.setId(cartao.getId());



    }

}
